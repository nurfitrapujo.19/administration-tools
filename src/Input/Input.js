import React from 'react';
import Styles from './Input.module.css';


const Input = (props) => {
    let output = null;
    switch (props.type) {
        case 'text' :
            output = <input type="text" name={props.name} className={Styles.text} onChange={props.change}></input>
            break;
        default :
            output = <input type="text" name={props.name} className={Styles.text} onChange={props.change}></input>
            break;
    }

    return output;
}

export default Input;