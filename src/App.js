import React, {Component} from 'react';
import Styles from './App.module.css';
import Card from './Card/Card';
import Input from './Input/Input';

class App extends Component {
  state = {
    persons : [
      {title : 'Fitra', text : 'this is card paragraph' },
      {title : 'Rexy', text : 'this is card paragraph' },
      {title : 'Fajar', text : 'this is card paragraph' }
    ],
    searchResult : []
  }

  searchChangeHandler = event => {
    let cards = null;
    const input = event.target.value;
    cards = this.matchString(input);
    this.setState({searchResult : cards});
  }

  matchString = (input) => {
    return this.state.persons.filter(person => person.title.toLocaleUpperCase().match(input.toLocaleUpperCase()));
  }

  cards = () => this.state.searchResult.map(person => {
    return(
      <Card
        img="https://images.vexels.com/media/users/3/145908/preview2/52eabf633ca6414e60a7677b0b917d92-male-avatar-maker.jpg"
        title={person.title}
        text={person.text}
      ></Card>
    ); 
  })

  render() {
    return (
      <div className={Styles.App}>
        <h1>Implementing search in React</h1>
        <div>
          <p>Input the name you want to search</p>
          <Input change={(event) => this.searchChangeHandler(event)}></Input>
        </div>
        <div>
          {this.cards()}
        </div>
      </div>
    );
  }
}

export default App;
