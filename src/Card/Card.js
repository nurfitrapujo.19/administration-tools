import React from 'react';
import Styles from "./Card.module.css";

const Card = props => {
    return (
        <div className={Styles.Card}>
            <img src={props.img} className={Styles.img} alt="person-avatar"></img> 
            <h1 className={Styles.Title}>{props.title}</h1>
            <p className={Styles.Text}>{props.text}</p>
        </div>
    );
}

export default Card;